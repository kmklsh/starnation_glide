package com.starnation.glide;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.starnation.glide.module.DefaultAppGlideModule;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * @author lsh
 * @since 15. 6. 6.
 */
public final class GlideLoader {

    //======================================================================
    // Variables
    //======================================================================

    private Object mErrorDrawable;

    private Object mDefaultImage;

    private OnSuccessListener mSuccessListener;

    private OnFailListener mFailListener;

    private boolean mCrossFade = true;

    private boolean mAutoImageResize = true;

    private float mThumbnail;

    private Transformation[] mTransformation;

    private boolean mSkipMemory;

    private int mOverrideWidth = Target.SIZE_ORIGINAL;

    private int mOverrideHeight = Target.SIZE_ORIGINAL;

    //======================================================================
    // Public Methods
    //======================================================================

    public GlideLoader crossFade(boolean crossFade) {
        mCrossFade = crossFade;
        return this;
    }

    public GlideLoader errorImage(int errorImage) {
        mErrorDrawable = errorImage;
        return this;
    }

    public GlideLoader errorImage(Drawable errorDrawable) {
        mErrorDrawable = errorDrawable;
        return this;
    }

    public GlideLoader defaultImage(int defaultImage) {
        mDefaultImage = defaultImage;
        return this;
    }

    public GlideLoader defaultImage(Drawable defaultImage) {
        mDefaultImage = defaultImage;
        return this;
    }

    public GlideLoader override(int overrideWidth, int overrideHeight) {
        mOverrideWidth = overrideWidth;
        mOverrideHeight = overrideHeight;
        return this;
    }

    public GlideLoader successListener(OnSuccessListener successListener) {
        mSuccessListener = successListener;
        return this;
    }

    public GlideLoader failListener(OnFailListener failListener) {
        mFailListener = failListener;
        return this;
    }

    public GlideLoader autoImageResize(boolean autoImageResize) {
        mAutoImageResize = autoImageResize;
        return this;
    }

    public GlideLoader transform(Transformation... transformation) {
        mTransformation = transformation;
        return this;
    }

    public GlideLoader skipMemory(boolean skipMemory) {
        mSkipMemory = skipMemory;
        return this;
    }

    public GlideLoader thumbnail(float thumbnail) {
        mThumbnail = thumbnail;
        return this;
    }

    @SuppressLint("CheckResult")
    public void load(String url, ImageView imageView) {
        String getUrl = getUrl(url);

        RequestOptions requestOptions = new RequestOptions();

        requestOptions.format(DecodeFormat.PREFER_ARGB_8888);
        requestOptions.disallowHardwareConfig();
        requestOptions.override(mOverrideWidth, mOverrideHeight);

        RequestManager manager;
        String type;
        if (imageView.getContext() instanceof Activity) {
            manager = Glide.with((Activity) imageView.getContext());
            type = "activity";
        } else {
            manager = Glide.with(imageView.getContext().getApplicationContext());
            type = "application";
        }

        if (DefaultAppGlideModule.isLoggable()) {
            Log.e("GLIDE", "load ->"
                    + " url : " + getUrl + " type : " + type);
        }

        RequestBuilder<Drawable> builder = manager
                .load(getUrl);

        if (mCrossFade == true) {
            builder.transition(DrawableTransitionOptions.withCrossFade());
        }

        if (mDefaultImage instanceof Integer) {
            requestOptions.placeholder((Integer) mDefaultImage);
            requestOptions.fallback((Integer) mDefaultImage);
        } else if (mDefaultImage instanceof Drawable) {
            requestOptions.placeholder((Drawable) mDefaultImage);
            requestOptions.fallback((Drawable) mDefaultImage);
        }

        if (mErrorDrawable instanceof Integer) {
            requestOptions.error((Integer) mErrorDrawable);
        } else if (mErrorDrawable instanceof Drawable) {
            requestOptions.error((Drawable) mErrorDrawable);
        }

        if (mSkipMemory == true) {
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        }

        if (mTransformation != null && mTransformation.length > 0) {
            requestOptions.transforms(mTransformation);
        }
        builder.apply(requestOptions);
        builder.listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                if (DefaultAppGlideModule.isLoggable()) {
                    Log.e("GLIDE", "onLoadFailed ->"
                            + " url : " + model);
                }
                return mFailListener != null && mFailListener.onLoadFailed(e, model, target, isFirstResource);
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                try {
                    if (DefaultAppGlideModule.isLoggable()) {
                        Log.d("GLIDE", "onResourceReady -> width : " + resource.getIntrinsicWidth()
                                + " height : " + resource.getIntrinsicHeight()
                                + " url : " + model);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return mSuccessListener != null && mSuccessListener.onResourceReady(resource, model, target, dataSource, isFirstResource);
            }
        });

        if (isThumbnail() == true) {
            builder.thumbnail(mThumbnail);
        }
        builder.into(imageView);
    }

    @Deprecated
    public void cancel() {
        /*if (mTarget != null) {
            Glide.clear(mTarget);
        }*/
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private boolean isThumbnail() {
        return mThumbnail != 0;
    }

    private static Drawable createDrawable(Context context, Object drawable) {
        try {
            if (drawable instanceof Integer) {
                int drawableRes = (int) drawable;
                if (drawableRes != 0) {
                    return ContextCompat.getDrawable(context, drawableRes);
                }
            } else if (drawable instanceof Drawable) {
                return (Drawable) drawable;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 한글이 들어간경우 인코딩 처리
     *
     * @param url
     *
     * @return
     */
    @SuppressWarnings("JavaDoc")
    private static String getUrl(String url) {
         /*
           2017. 11. 20. lsh
           1. http url 형식있는 url만 인코딩 처리.
           2. 저장소 파일경로는 인코딩 처리 제외.
        */
        if (isEmpty(url) == true) {
            return url;
        }
        if (isSuffixMatchHTTP(url) == true) {
            return getMatcherKoreaLanguageEncoding(url, "utf-8");
        }
        return url;
    }

    private static boolean isEmpty(String text) {
        return text == null || text.trim().length() <= 0;
    }

    private static boolean isSuffixMatchHTTP(String url) {
        return Pattern.compile("^(https?):\\/\\/").matcher(url).matches();
    }

    private static String getMatcherKoreaLanguageEncoding(String input, String charsetName) {
        if (isEmpty(input) == true) {
            return input;
        }
        Matcher matcher = Pattern.compile("[\\x{ac00}-\\x{d7af}]|[ㄱ-ㅎㅏ-ㅣ가-힣]").matcher(input);

        while (matcher.find()) {
            try {
                input = input.replaceAll(matcher.group(), URLEncoder.encode(matcher.group(), charsetName));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return input;
    }

    //======================================================================
    // OnResourceCallback
    //======================================================================

    public interface OnSuccessListener {
        /**
         * @param resource
         * @param model
         * @param target
         * @param dataSource
         * @param isFirstResource
         *
         * @return true 면 {@link com.bumptech.glide.request.target.ImageViewTarget} 호출을 안한다.
         *
         * @see RequestListener
         */
        boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource);
    }

    public interface OnFailListener {

        /**
         * @param e
         * @param model
         * @param target
         * @param isFirstResource
         *
         * @return true 면 {@link com.bumptech.glide.request.target.ImageViewTarget} 호출을 안한다.
         *
         * @see RequestListener
         */
        boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource);
    }
}
