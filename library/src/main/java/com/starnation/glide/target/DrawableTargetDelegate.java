package com.starnation.glide.target;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.starnation.glide.module.DefaultAppGlideModule;

/*
 * @author lsh
 * @since 2018. 7. 11.
 */
abstract class DrawableTargetDelegate {

    private final static String GLIDE = "GLIDE";

    private final static int ERROR_IMAGE = Integer.MIN_VALUE + 1;
    
    abstract ImageView view();
    
    abstract String url();

    public void onResourceReady(@NonNull Drawable resource) {
        try {
            if (DefaultAppGlideModule.isLoggable()) {
                Log.d(GLIDE, "onResourceReady -> width : " + resource.getIntrinsicWidth()
                        + " height : " + resource.getIntrinsicHeight()
                        + " url : " + url());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDrawable(Drawable drawable) {
        if (drawable != null) {
            if (DefaultAppGlideModule.isLoggable()) {
                Log.d(GLIDE, "setDrawable -> drawable : " + drawable);
            }
            view().setImageDrawable(drawable);
        }
    }

    public void onLoadStarted(Drawable placeholder) {
        if (placeholder != null) {
            if (DefaultAppGlideModule.isLoggable()) {
                Log.d(GLIDE, "onLoadStarted -> placeholder : " + placeholder + " url : " + url());
            }
            view().setImageDrawable(placeholder);
        }
    }

    public void onLoadFailed(Drawable errorDrawable) {
        if (DefaultAppGlideModule.isLoggable()) {
            Log.e(GLIDE, "onLoadFailed ->"
                    + " errorDrawable : " + errorDrawable
                    + " url : " + url());
        }
        if (errorDrawable != null) {
            view().setTag(ERROR_IMAGE, true);
            view().setImageDrawable(errorDrawable);
        }
    }

    public void onLoadCleared(Drawable placeholder) {
        if (placeholder != null) {
            if (DefaultAppGlideModule.isLoggable()) {
                Log.d(GLIDE, "onLoadCleared -> place holder : " + placeholder + " url : " + url());
            }
            view().setImageDrawable(placeholder);
        }
    }
    
    public void onDestroy() {
        if (DefaultAppGlideModule.isLoggable()) {
            Log.d(GLIDE, "onDestroy -> url : " + url());
        }
        try {
            if (view() != null) {
                Drawable drawable = view().getDrawable();
                if (drawable != null) {
                    drawable.setCallback(null);
                }
                view().destroyDrawingCache();
                view().setImageDrawable(null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
