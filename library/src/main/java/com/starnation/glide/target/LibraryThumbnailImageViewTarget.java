package com.starnation.glide.target;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.request.target.ThumbnailImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

/*
 * @author lsh
 * @since 2018. 7. 11.
 */
@Deprecated
public class LibraryThumbnailImageViewTarget extends ThumbnailImageViewTarget<Drawable>{

    //======================================================================
    // Constants
    //======================================================================

    private final static String GLIDE = "GLIDE";

    private final static int ERROR_IMAGE = Integer.MIN_VALUE + 1;

    //======================================================================
    // Variables
    //======================================================================

    private String mUrl;

    private DrawableTargetDelegate mDelegate = new DrawableTargetDelegate() {
        @Override
        ImageView view() {
            return view;
        }

        @Override
        String url() {
            return mUrl;
        }
    };

    //======================================================================
    // Constructor
    //======================================================================

    public LibraryThumbnailImageViewTarget(ImageView view, String url) {
        super(view);
        mUrl = url;
    }

    public LibraryThumbnailImageViewTarget(ImageView view, boolean waitForLayout) {
        super(view, waitForLayout);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Nullable
    @Override
    public Drawable getCurrentDrawable() {
        return super.getCurrentDrawable();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
        super.onResourceReady(resource, transition);
        mDelegate.onResourceReady(resource);
    }

    @Override
    protected Drawable getDrawable(Drawable resource) {
        return resource;
    }

    @Override
    public void setDrawable(Drawable drawable) {
        super.setDrawable(drawable);
        mDelegate.setDrawable(drawable);
    }

    @Override
    public void onLoadStarted(@Nullable Drawable placeholder) {
        super.onLoadStarted(placeholder);
        mDelegate.onLoadStarted(placeholder);
    }

    @Override
    public void onLoadFailed(@Nullable Drawable errorDrawable) {
        super.onLoadFailed(errorDrawable);
        mDelegate.onLoadFailed(errorDrawable);
    }

    @Override
    public void onLoadCleared(@Nullable Drawable placeholder) {
        super.onLoadCleared(placeholder);
        mDelegate.onLoadCleared(placeholder);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDelegate.onDestroy();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static boolean isErrorImage(View view) {
        return view.getTag(ERROR_IMAGE) instanceof Boolean && (boolean) view.getTag(ERROR_IMAGE);
    }
}
