package com.starnation.glide.module;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.ImageViewTarget;

/*
 * @author lsh
 * @since 15. 6. 5.
 */
public class DefaultAppGlideModule extends com.bumptech.glide.module.AppGlideModule {

    //======================================================================
    // Constants
    //======================================================================

    private static final int DISK_CACHE_SIZE = 250 * 1024 * 1024; // 250MB

    private static final String DIRECTORY = "bookmac";

    //======================================================================
    // Variables
    //======================================================================

    private static boolean sLoggable;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void applyOptions(final Context context, GlideBuilder builder) {
        if (DefaultAppGlideModule.isLoggable()) {
            Log.d("GLIDE", "DefaultAppGlideModule applyOptions");
        }
        try {
            ImageViewTarget.setTagId(DIRECTORY.hashCode());
        } catch (Exception e) {
            e.printStackTrace();
        }
        builder.setDiskCache(new InternalCacheDiskCacheFactory(context, DIRECTORY, DISK_CACHE_SIZE));
        MemorySizeCalculator calculator = new MemorySizeCalculator.Builder(context).build();
        builder.setMemoryCache(new LruResourceCache(calculator.getMemoryCacheSize()));
        builder.setBitmapPool(new LruBitmapPool(calculator.getBitmapPoolSize()));

        builder.setDefaultRequestOptions(new RequestOptions()
                .format(DecodeFormat.PREFER_ARGB_8888));

        builder.setLogLevel(Log.DEBUG);
    }

    @Override
    public void registerComponents(@NonNull Context context, @NonNull Glide glide, @NonNull Registry registry) {
        if (DefaultAppGlideModule.isLoggable()) {
            Log.d("GLIDE", "DefaultAppGlideModule registerComponents");
        }
        glide.setMemoryCategory(MemoryCategory.HIGH);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static boolean isLoggable() {
        return sLoggable;
    }

    public static void setLoggable(boolean loggable) {
        sLoggable = loggable;
    }
}
