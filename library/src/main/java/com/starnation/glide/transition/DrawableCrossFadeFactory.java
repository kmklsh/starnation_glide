package com.starnation.glide.transition;

import android.graphics.drawable.Drawable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.request.transition.DrawableCrossFadeTransition;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.request.transition.TransitionFactory;

/*
 * @author lsh
 * @since 2018. 8. 7.
 */
public class DrawableCrossFadeFactory implements TransitionFactory<Drawable> {

    private final int duration;
    private final boolean isCrossFadeEnabled;
    private DrawableCrossFadeTransition resourceTransition;

    public DrawableCrossFadeFactory(int duration, boolean isCrossFadeEnabled) {
        this.duration = duration;
        this.isCrossFadeEnabled = isCrossFadeEnabled;
    }

    @Override
    public Transition<Drawable> build(DataSource dataSource, boolean isFirstResource) {
        return getResourceTransition();
    }

    protected Transition<Drawable> getResourceTransition() {
        if (resourceTransition == null) {
            resourceTransition = new DrawableCrossFadeTransition(duration, isCrossFadeEnabled);
        }
        return resourceTransition;
    }
}
