package com.starnation.glide.transformation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.starnation.glide.module.DefaultAppGlideModule;

import java.security.MessageDigest;

/*
 * @author lsh
 * @since 2016. 8. 2.
 */
public class RoundedBitmapTransformation implements Transformation<Bitmap> {

    //======================================================================
    // Variables
    //======================================================================

    private final Builder mBuilder;

    private final Paint mBitmapPaint;

    private final Paint mContentPaint;

    private final Paint mBorderPaint;

    private final RectF mBorderRect = new RectF();

    private final Matrix mShaderMatrix = new Matrix();

    private final RectF mBounds = new RectF();

    private final RectF mDrawableRect = new RectF();

    private final RectF mDrawableContent = new RectF();

    private final RectF mBitmapRect = new RectF();

    private final BitmapPool mBitmapPool;

    //======================================================================
    // Constructor
    //======================================================================

    RoundedBitmapTransformation(Context context, Builder builder) {
        mBitmapPool = Glide.get(context).getBitmapPool();
        mBuilder = builder;

        mBitmapPaint = new Paint();
        mBitmapPaint.setStyle(Paint.Style.FILL);
        mBitmapPaint.setAntiAlias(true);

        mContentPaint = new Paint();
        mContentPaint.setStyle(Paint.Style.FILL);
        mContentPaint.setColor(Color.TRANSPARENT);
        mContentPaint.setAntiAlias(true);

        mBorderPaint = new Paint();
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setColor(mBuilder.mBorderColor);
        mBorderPaint.setStrokeWidth(mBuilder.mBorderWidth);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @NonNull
    @Override
    public Resource<Bitmap> transform(@NonNull Context context, @NonNull Resource<Bitmap> resource, int outWidth, int outHeight) {
        Bitmap toTransform = resource.get();
        Bitmap buffer = mBitmapPool.get(outWidth, outHeight, Bitmap.Config.ARGB_8888);

        if (DefaultAppGlideModule.isLoggable()) {
            Log.d("GLIDE", "transform -> out w:" + outWidth + "/h:" + outHeight + " bitmap src w:" + toTransform.getWidth() + "h:" + toTransform.getHeight() + " buffer : " + (buffer != null));
        }

        if (toTransform.getWidth() > outWidth || toTransform.getHeight() > outHeight) {
            Bitmap temp = resource.get();
            toTransform = createScaledBitmap(temp, outWidth, outHeight, true);
        }
        if (buffer == null) {
            buffer = Bitmap.createBitmap(outWidth, outHeight, Bitmap.Config.ARGB_8888);
        }

        try {
            setScale(buffer);

            Canvas canvas = new Canvas(buffer);

            //Modify 2016. 8. 2. lsh BitmapShader
            BitmapShader bitmapShader = new BitmapShader(toTransform, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            bitmapShader.setLocalMatrix(mShaderMatrix);
            mBitmapPaint.setShader(bitmapShader);

            //Modify 2016. 8. 2. lsh 둥근 모양
            if (mBuilder.mOver == true) {
                canvas.drawOval(mDrawableRect, mBitmapPaint);
                if (mContentPaint.getColor() != Color.TRANSPARENT) {
                    canvas.drawOval(mDrawableContent, mContentPaint);
                }
                canvas.drawOval(mBorderRect, mBorderPaint);
            } else {
                //Modify 2016. 8. 2. lsh Rounded
                float radius = Math.max(mBuilder.mDefaultRadius, 0);

                canvas.drawRoundRect(mDrawableRect, radius, radius, mBitmapPaint);

                if (mContentPaint.getColor() != Color.TRANSPARENT) {
                    canvas.drawRoundRect(mDrawableContent, radius, radius, mContentPaint);
                }

                canvas.drawRoundRect(mBorderRect, radius, radius, mBorderPaint);
            }
        } catch (Exception e) {
            if (DefaultAppGlideModule.isLoggable()) {
                e.printStackTrace();
            }
        }
        return BitmapResource.obtain(buffer, mBitmapPool);
    }

    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {

    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static Bitmap createScaledBitmap(@NonNull Bitmap src, int dstWidth, int dstHeight,
                                             boolean filter) {
        Matrix m = new Matrix();

        final int width = src.getWidth();
        final int height = src.getHeight();
        if (width != dstWidth || height != dstHeight) {
            final float sx = dstWidth / (float) width;
            final float sy = dstHeight / (float) height;
            m.postScale(sx, sy);
        }
        return Bitmap.createBitmap(src, 0, 0, width, height, m, filter);
    }

    private void setScale(Bitmap bitmap) {
        mBitmapRect.set(0, 0, bitmap.getWidth(), bitmap.getHeight());
        mBounds.set(0, 0, bitmap.getWidth(), bitmap.getHeight());

        mBorderRect.set(mBitmapRect);
        mShaderMatrix.setRectToRect(mBitmapRect, mBounds, Matrix.ScaleToFit.CENTER);
        mShaderMatrix.mapRect(mBorderRect);
        mBorderRect.inset((mBuilder.mBorderWidth) / 2, (mBuilder.mBorderWidth) / 2);
        mShaderMatrix.setRectToRect(mBitmapRect, mBorderRect, Matrix.ScaleToFit.FILL);

        mDrawableRect.set(mBorderRect);
        mDrawableContent.set(mBorderRect);
    }

    //======================================================================
    // Builder
    //======================================================================

    public final static class Builder {

        float mDefaultRadius;

        float mBorderWidth;

        @ColorInt
        int mBorderColor;

        @ColorInt
        int mContentBackgroundColor;

        boolean mOver;

        public Builder defaultRadius(float defaultRadius) {
            mDefaultRadius = defaultRadius;
            return this;
        }

        public Builder borderWidth(float borderWidth) {
            mBorderWidth = borderWidth;
            return this;
        }

        public Builder borderColor(@ColorInt int borderColor) {
            mBorderColor = borderColor;
            return this;
        }

        public Builder contentBackgroundColor(@ColorInt int contentBackgroundColor) {
            mContentBackgroundColor = contentBackgroundColor;
            return this;
        }

        public Builder over(boolean over) {
            mOver = over;
            return this;
        }

        public RoundedBitmapTransformation build(@NonNull Context context) {
            return new RoundedBitmapTransformation(context, this);
        }
    }
}
