package com.starnation.glide.transformation;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.starnation.glide.module.DefaultAppGlideModule;

import java.security.MessageDigest;

/*
 * @author lsh
 * @since 2016. 2. 26.
 */
public abstract class BlurBitmapTransformation extends BitmapTransformation {

    //======================================================================
    // Constants
    //======================================================================

    private static final int DEFAULT_BLUR_RADIUS = 25;

    //======================================================================
    // Variables
    //======================================================================

    private int mRadius = DEFAULT_BLUR_RADIUS;

    //======================================================================
    // Constructor
    //======================================================================

    public static BlurBitmapTransformation create(final Context context) {
        return new BlurBitmapTransformation(DEFAULT_BLUR_RADIUS) {
            @Override
            public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {

            }

            @Override
            public Context getContext() {
                return context;
            }
        };
    }

    public BlurBitmapTransformation(int radius) {
        mRadius = radius;
    }

    //======================================================================
    // Abstract Methods
    //======================================================================

    public abstract Context getContext();

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {

    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        return createBlurBitmap(Bitmap.createScaledBitmap(toTransform, outWidth, outHeight, true));
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void valid(Bitmap bitmap) throws Exception {
        if (bitmap == null) {
            throw new NullPointerException("bitmap null");
        }
    }

    private Bitmap createBlurBitmap(Bitmap src) {
        try {
            valid(src);
            try {
                //Modify 15. 6. 15. lsh SM-N90L Crash bug 발생
                Bitmap bitmap = src;
                RenderScript rs = RenderScript.create(getContext());
                Allocation input = Allocation.createFromBitmap(rs, src, Allocation.MipmapControl.MIPMAP_NONE, Allocation.USAGE_SCRIPT);
                Allocation output = Allocation.createTyped(rs, input.getType());
                ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
                script.setRadius(mRadius /* e.g. 3.f */);
                script.setInput(input);
                script.forEach(output);
                output.copyTo(bitmap);
                script.destroy();
                output.destroy();
                return bitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        } catch (Exception e) {
            if (DefaultAppGlideModule.isLoggable()) {
                e.printStackTrace();
            }
        }
        return src;
    }
}
