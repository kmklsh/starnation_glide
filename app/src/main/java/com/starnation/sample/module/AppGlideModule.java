package com.starnation.sample.module;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.starnation.glide.BitmapBytesTranscoder;
import com.starnation.glide.module.DefaultAppGlideModule;

/*
 * @author lsh
 * @since 15. 6. 5.
 */
@GlideModule
public class AppGlideModule extends DefaultAppGlideModule {

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        setLoggable(true);
        super.applyOptions(context, builder);
    }

    @Override
    public void registerComponents(@NonNull Context context, @NonNull Glide glide, @NonNull Registry registry) {
        super.registerComponents(context, glide, registry);
        int size = 500;
        GlideUtil.replaceTransCoder(glide, new BitmapBytesTranscoder(Bitmap.CompressFormat.PNG, 100, size, size, context));

       /* registry.prepend()

        registry.replace(Bitmap.class, BitmapDrawable.class, new ModelLoaderFactory<Bitmap, BitmapDrawable>() {
            @NonNull
            @Override
            public ModelLoader<Bitmap, BitmapDrawable> build(@NonNull MultiModelLoaderFactory multiFactory) {
                return null;
            }

            @Override
            public void teardown() {

            }
        });*/
    }
}
