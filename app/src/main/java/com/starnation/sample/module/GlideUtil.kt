package com.starnation.sample.module

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder

/*
 * @author lsh
 * @since 2018. 9. 12.
*/
object GlideUtil {

    @JvmStatic
    fun replaceTransCoder(glide: Glide, resourceTranscoder: ResourceTranscoder<Bitmap, BitmapDrawable>) {
        val remove = glide.registry.removeTrans(Bitmap::class.java, BitmapDrawable::class.java)
        if (remove) {
            glide.registry.register(Bitmap::class.java, BitmapDrawable::class.java, resourceTranscoder)
        }
    }
}