package com.starnation.sample.module

import com.bumptech.glide.Registry
import com.bumptech.glide.load.resource.transcode.TranscoderRegistry

/*
 * @author lsh
 * @since 18/10/2018
*/
inline fun Registry.removeTrans(resourceClass: Class<*>, transcodedClass: Class<*>): Boolean {
    this.imageHeaderParsers

    val field = this.javaClass.getDeclaredField("transcoderRegistry")
    field.isAccessible = true
    val value = field.get(this)

    if (value is TranscoderRegistry) {
        val result = value.remove(resourceClass, transcodedClass)
        return result
    }
    return false
}

inline fun <Z, R> TranscoderRegistry.remove(resourceClass: Class<Z>, transcodedClass: Class<R>): Boolean {
    val field = this.javaClass.getDeclaredField("transcoders")
    field.isAccessible = true
    val value = field.get(this)

    if (value is ArrayList<*>) {
        val iterable = value.iterator()
        while (iterable.hasNext()) {
            val item = iterable.next()
            val method = item.javaClass.getDeclaredMethod("handles", Class::class.java, Class::class.java)
            val result = method.invoke(item, resourceClass, transcodedClass)
            if (result == true) {
                iterable.remove()
                return true
            }
        }
    }
    return false
}