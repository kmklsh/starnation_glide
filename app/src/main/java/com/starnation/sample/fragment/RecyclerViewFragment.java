package com.starnation.sample.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starnation.sample.R;
import com.starnation.sample.ui.OnViewHolderEventListener;

/*
 * @author lsh
 * @since 2017. 3. 21.
 */
public abstract class RecyclerViewFragment extends Fragment implements OnViewHolderEventListener {

    //======================================================================
    // Variables
    //======================================================================

    RecyclerView mRecyclerView;

    Toolbar mToolbar;

    //======================================================================
    // Abstract Methods
    //======================================================================

    public abstract RecyclerView.LayoutManager onCreateLayoutManager();

    public abstract RecyclerView.Adapter onCreateRecyclerViewAdapter();

    //======================================================================
    // Override Methods
    //======================================================================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_galley_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);

        if (getActivity() instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            activity.setSupportActionBar(mToolbar);
        }
        setUpActionbar();

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        mRecyclerView.setAdapter(onCreateRecyclerViewAdapter());
        mRecyclerView.setLayoutManager(onCreateLayoutManager());
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public void scrollTop() {
        if (mRecyclerView == null) {
            return;
        }

        RecyclerView.LayoutManager manager = mRecyclerView.getLayoutManager();
        if (manager == null) {
            return;
        }

        mRecyclerView.smoothScrollToPosition(0);
    }

    public void setTitle(String title) {
        if (mToolbar != null) {
            if (getActivity() instanceof AppCompatActivity) {
                AppCompatActivity activity = (AppCompatActivity) getActivity();
                ActionBar actionBar = activity.getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setTitle(title);
                }
                mToolbar.setTitle(title);
            }
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void setUpActionbar() {
        if (getActivity() instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            ActionBar actionBar = activity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowHomeEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }
}
