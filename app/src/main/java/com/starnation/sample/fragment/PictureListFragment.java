package com.starnation.sample.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.starnation.sample.R;
import com.starnation.sample.ui.PictureViewHolder;
import com.starnation.widget.recyclerview.RecyclerViewAdapter;
import com.starnation.widget.recyclerview.RecyclerViewHolder;

/*
 * @author lsh
 * @since 2017. 3. 21.
 */
public class PictureListFragment extends RecyclerViewFragment {

    //======================================================================
    // Constants
    //======================================================================

    private final static int ITEM_PICTURE = 99;

    private final static int ITEM_PICK = 98;

    private static String IMAGES[] = {
            "https://s3.ap-northeast-2.amazonaws.com/bookmac-image/user/event/event243_middle_banner.png",
            "http://www.city.kr/files/attach/images/1326/542/186/010/9bbd68054422876eb730b296963d0e82.jpg",
            "https://post-phinf.pstatic.net/MjAxNzExMThfMTU3/MDAxNTEwOTg1MjU0MDMw.UnE6KDZeDvuggh3tCZyAh1TnZdmvZQGLVZbT2nhBIAYg.9QvOIhmCZnSwa0BXJ8CVs3ROxovGtlVmRmglS2HE8esg.JPEG/20171118-006.jpg?type=w1200",
            "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0NDQ0NDRANDQ0NDQ0NDQ0NDQ8NDQ0NFREWFhURFRUYHSggGBomGxUVITUtJSkrLi4vFx8zODMtNygtLisBCgoKDg0OFQ8PFSsdFR0tLS0rKysrKy0rLSsrLS0tKy0rLSs3Ky0tLS0tLSstLSstKy0rKy0tLS0rLSsrLS0rLf/AABEIAKgBLAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAQIDBAYFB//EADwQAAICAgECBAQCCAENAAAAAAABAhEDEgQTIQUGMUFRYYGRcaEUIiMyQlKxwXIHFjNDU2KCkpOi0dLx/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAjEQEAAgIBBAIDAQAAAAAAAAAAARESEwMCITFhUVIjMkJB/9oADAMBAAIRAxEAPwD4kMQz0BDACgAKAIAGACAYAIYUMBAMAEAwAQDABASoAEA6ABCJABECQUBECVBQEQJABECVCoBAOgoKQiQUBEB0FAIYDAQDoKCEMdBQCChgAhjABAOgAQUSoCiNDoYUAqAlQONNp+q9SIiFEqGot9l3fsgIBRKgoFo0BKgoojQUSoKAiBKgoKiBKgIIgOgooQiQARoKJUIgQiVBQUqCh0OioQUSoKAiFEqHQEaCiVBQRGh0Oh0BGgolQUBGh0TjBtpJNttJJerbdJI2eLeF5OJkWLLq5OKlcG3G/dJ+9PsEZcPGnOOScYtxwxU8kvRRi2kvq2y7wrFCefHGdOLcqUnUZT1bhFv4OWq+pp8J58MWPk4MsZSxcqEIyljrqY5QbcZJPs/V/kec16+6+LVWvwJ8qE5RmpNVKMk2mtakndNewpJX2uva/WvmSdvu7b+fcVFRGiWOrTbaSabr976fMKHQEZu25dlbb7eit39jT4jhUMmqWt48Epw7/s8ksUZTj3+Db/D09imLadptNd006af4ifxfdu237tgR0dOVPVNRcqdKTTaV/F0/s/gKj2f0rBi8PycaEurm5OXFlytQlGGGOO3GKckrlb9u3d9/Qx+FeH5OXnx8fHSnkbScr1ikm23Xt2IMVBRo5fFngy5MOVa5MU5QnH1qSf8AQpoojQUSoKAhQUToKCoUFEqCgI0FEqCgIUFE6FQEaFROgoKiOhpDoIjQ6JUFARodEqCgI0FE6CiojQUToKAjR6XgHhX6ZneLZwjDFPLOSW0tU0qS+NyRm4vDy5pOOKEsklFyaiv3Yr3b9l3L8M+Twc7dSw5oxcZQyR7ShL1TT9U+32RJ9eQeKeG5eDnjFyTa1y4ssV2dO06fun7F/jvjP6d0pTxrHlhvtKMm4y2r0T7pWvS3RV4p4pm5bg8ui6akorHFxXerbttv0XvXYw6iI8TPkv4QoKJ6j1NIhQUT1HQFdBRZQUBChUWahQFdHp+A+JQ4eWeWWN5W8bhFKSjVtN96dXVenxMGoUSYsX5p5ubyZSrbNyMjdLsrft39El8fZGnx7wLLwJYo5ZQms0HKMsd63F1KPf4WvuQ8I574uXqqEcj1cak3Grrun8e3w92X+J87P4jlxpQ/cjKOPFD9ZxT7yk37t0u/ZUkTvfpXj0FG3n+HZ+NKMc+OWJyW0dqqS96a7P2+5loqIUKiygoCuhUWUFAV0FFlCoKhQqLKFQEKAnQqClQ6GkNICNDolQ6KiNBRNIKAjQak6HQpENQoso3eC+HfpXIhhcnCLU5TkqclCMW3V9r7Cew0eWPFYcSeZZU3DNHGt4rZwlCTa7e6dv079kS80+K4+ZlxvHs1jhKLnJVtcrpX3pfP3b+vpeYPLnHwcVcjBKSlB445cU5bpqTpOLr1uvlXw9+X1MdMdMzlCzcdlaQ6JpDo6Mq6Ciyh6gtXQalmo9QivUKLNQ1Cq6CizUeoLVahqWaj1BarU9Xy54hj4uWcssW45MemyW0oPZO6+Hb8fQ87UNSTFxRb3PNXjkeZHDjx7Shilkm5yVXKSSUY33pJd79W/lb57U7Hyx4NxZ4IZ88Vlc55U026hCPaqXv738GjxPMXAxcbl5cWFt40scoqXeUNoKWjfyv7UY6Zi8Y/xZvy8mhNFuotTaK6FRbqLUKroVFuotQK6FRbqJxAqoKLKFqFslEaRNRHqBDUNS1RHqEV0PUs1GolFeoalmpJRCKtS3jZZ4pxyY5OE4O4yVWu1f0bX1HqNRA1c/xXkciKhkktItS0jHVOSTSk/j2b+Rh1LdR6iIrwkyq1DUu0HoVFOoal2g9AKdQ1LtB6AU6hqX6BoBRqGpfoGgFGoal+gaAUahqX6BoAcXl5sN9KcoJ92lTi38afayjI5SblJuUpNuUpNylJv1bb9S7QWhKW1GotTRoLQCjUNS7UHEKo1FqX6C1AocRal+otQqjUNS7UWpBFRHqWqJJRIqpRGolqiSUSoqUB6lygSUAKNBqBfoPQqKVAeheoD0CKNB6F6gS0CM6gPQ0KA9AM6gPQ0aD0KM2g9DRoPQIz6BoadA6YGbQeho6YdMDNoGhp6YdMKzaC0NWgumQZtBaGrpi0AzaC0NOgaEGXQWhqcCLgFZnATgadBaBWZwFoaXAi4EsZtBaGlwFoLVWoElAtUCagYtqlSgSUC5QJKBbKULGSUDQoElAWlM6gSWM0KBJYy2lM6xjWM0rGSWMWlMyxjWM1LGNYy2lMyxj6ZqWMaxi0ZemPpmrpj6ZbGXph0zX0x9MWjJ0x9M19MOmSxk6YdM2dMOmLGPph0zZ0xdIWMfTDpmzpC6QtWPpi6Zt6ZHpkspj6YnjNnTE8ZLWmN4xOBreMTxi1pjcBPGa3jE8ZLWmN4xOBreMi4EyWmRwI6GtwIvGMlpUok1AnGJZGJyzdMVagTUC1QJqAzMVSgSUC5QJqAzMVKxkljL1AmoDNMGdYyaxmhYySxjNMGdYxrGaljJLGXYmDKsZLpmlYySxjNMGXpj6RrWIfTLmYMnSH0zX0x9MZpgydIfSNfTDpjMwZOkHSNnTDpk2GDH0g6Rs6YumNhgx9IOkbOmJ4ybGsGPpkembemJ4xsMGJ4yLxm14yDxk2LgxvGReM2PGReMmxcGRwIOBscCDgTYuDI4EHA2OBBwGxcGRwIamtwK3AZmDxY+N8X/aP/pz/APBpw+LcWXplgq/muH9aOGA1rj5XP072Hi3EbrrY7XxdL7vsbYcjE1anja9b3jVHzUVGZ4vaxyen0LN43w8b1lmg3/uKWRL6xTI8nzDwsaT6nUbVqOJbP+yX1ZwAF1R8ps9O4h5u4l94chfNwh2+0izH5u4bTbWaLT7ReNNtfFU6/M4MC6oNku0zedsSa6eCco+7nOON/ZJ/1LsPnbjP9/Fmj/h0nX5o4UBr6Uzl9Lfmjw9RUutd/wAKx5Nl+KrsYeZ5248O2DHkzP4yrFD+7/I4ICRxQTyS7bD57j36nHd120yppv52uxn8S88ZJx142NYm/XJkaySX4KqX1s5Ggo1r6Uyl6PN8b5meChlzTlBO9VrBN/PVK/qavCvNPM436u3Whd65nKddvaV2jxKA1jHwly63g+eeRGT6+PHlg36Q/ZSgr9vW1+P3OiwecfDpRUpZJ43daTxTc18/1U1X1PmAzM8cSsdUw+08TkYs0FkxTjkhJWpRd/8Awtm4xVyaivS5NJHxficrLgmsmGcsc1/FBtOvg/ivxHyuVlzS3zTnkk/4pycvor9Ec9M357NZ9NeH17n+IYOPiebLOKgk9aabm6vWK92eH4b514eaWmVT4/8ALKbUsb7e7Xo/p9T5tQzUcPbvKT1/EPs/B5mDkw6mCccsLpuL9H8GvVfU0aHxbjcrLhe2LJkxytO8c5Qtr0uvX3Oj4fnnmQSWSOLMlSbaeOb+q7fkcuvi64/Xu6dPX0T+3Z9G1E4nGy/ygQ1Vcebn/Enlior8HVv7Bn8/Q7dPjzl8epkjDv8ARM5Yc31dvxfZ2OpFxOLh5+/m4zu++uftX1ia4+eOI/WHIj/wQf8ASRJ6eWP5I1T/AE6Zoi4niYfNfBn/AKxw7XU8c1/ajPyPOXEi6gsuX5wgor/uaZj8k9sZaw44i8odC4kHE5KXnnu64719m81Ovw1FyvOyr9jhe3bvlkqX0j6/dG9fN9Uvi+zrHEg0cPl848uX7scMF/hlK/uyifmvmu/1sav4Y12/CzccPL6Z2cTvGiDRwuLzTzIx1csc37Snj/WX2aX5BPzVzHX+iXzWP1+7Gnl9GzidtJFbOHfmPmOSl1F2/h0hq/yv8yz/ADp5Pww/8kv/AGLp5DZxvDGAHreQgGBQrAAALAYAKwsYAAWABBYAAsFhYwFhWPYAFlDZBsgAllDYNgAtg2DYAAewbAAD2DYAANhbAABuLcAIo3FsMBYWwWAAIAAK/9k=",
            "http://www.city.kr/files/attach/images/1326/574/986/008/952d7f297c54d0e36be3a563963cf428.jpg",
            "https://t1.daumcdn.net/cfile/tistory/232638475974F82B1A",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTNqCk--MAYUQxxdDZIrK3kFDqMfZFd458y0_o8PdvJ5aMNauyJfw",
            "https://t1.daumcdn.net/cfile/tistory/2747D54455A2315C2A",
            "http://2.bp.blogspot.com/-MGo5sgFzvL0/T-r0t4xe4HI/AAAAAAAAABw/MitHl2HKTuk/s1600/11-1s+-1t-1.jpg",
            "http://www.kang-g.com/cheditor/attach/JNCXuk1zouM.jpg"
    };

    //======================================================================
    // Override Methods
    //======================================================================


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_picture_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_ok) {
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public RecyclerView.LayoutManager onCreateLayoutManager() {
        return new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
    }

    @Override
    public RecyclerView.Adapter onCreateRecyclerViewAdapter() {
        return new RecyclerViewAdapter() {
            @Override
            public int getItemHeaderCount() {
                return 0;
            }

            @Override
            public int getSupportItemCount() {
                try {
                    return IMAGES.length;
                } catch (Exception e) {
                    // Nothing
                }
                return 0;
            }

            @Override
            public int getSupportItemViewType(int position) {
                if (position < getItemHeaderCount()) {
                    return ITEM_PICK;
                }
                return ITEM_PICTURE;
            }

            @Override
            public Object getSupportItem(int viewType, int position) {
                try {
                    return IMAGES[position];
                } catch (Exception e) {
                    // Nothing
                }
                return null;
            }

            @Override
            public RecyclerViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
                return new PictureViewHolder(parent, PictureListFragment.this);
            }
        };
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onViewHolderEvent(RecyclerViewHolder holder, int id) {

    }
}
