package com.starnation.sample.ui;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.request.target.Target;
import com.starnation.glide.GlideLoader;
import com.starnation.glide.transformation.RoundedBitmapTransformation;
import com.starnation.sample.R;
import com.starnation.widget.recyclerview.holder.SelectorViewHolder;

/*
 * @author lsh
 * @since 2017. 3. 21.
*/
public final class PictureViewHolder extends SelectorViewHolder<String> {

    //======================================================================
    // Variables
    //======================================================================

    PictureImageView mImageView;

    final OnViewHolderEventListener mListener;

    //======================================================================
    // Constructor
    //======================================================================

    public PictureViewHolder(@NonNull ViewGroup viewGroup, OnViewHolderEventListener listener) {
        super(viewGroup, R.layout.viewholder_picture);
        mListener = listener;

        mImageView = (PictureImageView) findViewById(R.id.imageview);
        mImageView.setRoundedBitmapTransformation(new RoundedBitmapTransformation.Builder().defaultRadius(getResources().getDimension(R.dimen.cardview_default_radius)).build(getContext()));
        mImageView.setOnClickListener(createOnClickListener());
        mImageView.setOnSuccessListener(new GlideLoader.OnSuccessListener() {
            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                Log.d("GLIDE", "onResourceReady > model : "+model+" target : "+target+ " dataSource : "+dataSource+" isFirstResource : "+isFirstResource);
                return false;
            }
        });
    }

    private View.OnClickListener createOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String picture = getItem();

                if (picture == null) {
                    return;
                }
                if (mListener != null) {
                    mListener.onViewHolderEvent(PictureViewHolder.this, v.getId());
                }
            }
        };
    }

    //======================================================================
    // Override Methods
    //======================================================================


    @Override
    public void onRefresh(String picture) {
        super.onRefresh(picture);
        if (picture != null) {
            mImageView.setImageUrl(picture);
        }
    }

    @Override
    public void release() {
        super.release();
        if (mImageView != null){
            mImageView.releaseImage();
        }
    }
}
