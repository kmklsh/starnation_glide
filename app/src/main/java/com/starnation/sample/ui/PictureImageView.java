package com.starnation.sample.ui;

import android.content.Context;
import android.util.AttributeSet;

/*
 * @author lsh
 * @since 2017. 3. 21.
*/
public class PictureImageView extends com.starnation.widget.BaseImageView {

    //======================================================================
    // Constructor
    //======================================================================

    public PictureImageView(Context context) {
        super(context);
    }

    public PictureImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PictureImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
