package com.starnation.sample.ui;

import com.starnation.widget.recyclerview.RecyclerViewHolder;

/*
 * @author lsh
 * @since 2017. 3. 22.
*/
public interface OnViewHolderEventListener {

    void onViewHolderEvent(RecyclerViewHolder holder, int id);
}
